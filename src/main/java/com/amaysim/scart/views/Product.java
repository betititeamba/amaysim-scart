package com.amaysim.scart.views;

public class Product {
	public static final String UNLIMITED_1GB_CODE = "ult_small";
	public static final String UNLIMITED_2GB_CODE = "ult_medium";
	public static final String UNLIMITED_5GB_CODE = "ult_large";
	public static final String ONE_GB_CODE = "1gb";

	private String code;
	private String name;
	private Double price;

	public Product() {
		super();
	}

	public Product(String code) {
		this();
		this.code = code;
	}

	public Product(String code, String name, Double price) {
		this(code);
		this.name = name;
		this.price = price;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}

}
