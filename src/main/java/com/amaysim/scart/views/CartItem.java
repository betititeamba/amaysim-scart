package com.amaysim.scart.views;

import java.util.HashSet;
import java.util.Set;

import com.amaysim.scart.businessrules.PricingRule;

public class CartItem implements Cloneable {
	private Product product;
	private Set<PricingRule> appliedPricingRules = new HashSet<>();

	public CartItem(Product product) {
		super();
		this.product = product;
	}

	public Product getProduct() {
		return product;
	}

	public Double getPrice() {
		PriceCalculator calculator = new PriceCalculator(product.getPrice(), appliedPricingRules);
		return calculator.calculate();
	}

	public void addPricingRule(PricingRule rule) {
		appliedPricingRules.add(rule);
	}

	public CartItem copy() {
		try {
			return (CartItem) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}

	private static class PriceCalculator {
		private Double price;
		private Set<PricingRule> rules;

		public PriceCalculator(Double price, Set<PricingRule> rules) {
			this.price = price;
			this.rules = rules;
		}

		private void aggregate(PricingRule r) {
			this.price = r.calculate(this.price);
		}

		public Double calculate() {
			rules.forEach(this::aggregate);
			return price;
		}

	}
}
