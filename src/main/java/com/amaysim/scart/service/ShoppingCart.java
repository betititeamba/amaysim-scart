package com.amaysim.scart.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.amaysim.scart.businessrules.PricingRule;
import com.amaysim.scart.collector.CloningCollector;
import com.amaysim.scart.repository.PricingRulesRepository;
import com.amaysim.scart.views.CartItem;
import com.amaysim.scart.views.Product;

public class ShoppingCart {
	private static final PricingRulesRepository pricingRulesRepository = PricingRulesRepository.getInstance();

	private List<CartItem> cartItems = new ArrayList<>();
	private Set<PricingRule> pricingRules;

	private ShoppingCart(Set<PricingRule> pricingRules) {
		this.pricingRules = pricingRules;
	}

	public static ShoppingCart newInstance(Set<PricingRule> pricingRules) {
		return new ShoppingCart(pricingRules);
	}

	public static ShoppingCart newInstance() {
		return newInstance(new HashSet<>(pricingRulesRepository.getAllAuto()));
	}

	public void add(Product p, String... promoCodes) {
		addToCart(p);
		addToPricingRules(promoCodes);
	}

	public Double total() {
		return calculateTotal(items());
	}

	public List<CartItem> items() {
		List<CartItem> clonedItems = cloneCarts();
		loadPricingRules(clonedItems);
		return clonedItems;
	}

	private List<CartItem> cloneCarts() {
		// Clone first so that we will not edit the original one
		// This is for the purpose of re calculate
		return cartItems.stream().collect(CloningCollector.newInstance());
	}

	private void loadPricingRules(List<CartItem> clonedItems) {
		// Load pricing rules
		pricingRules.forEach((r) -> {
			r.identify(clonedItems);
		});
	}

	private Double calculateTotal(List<CartItem> clonedItems) {
		// calculate total price
		return clonedItems.stream().mapToDouble(CartItem::getPrice).sum();
	}

	private void addToCart(Product p) {
		if (p != null) {
			cartItems.add(new CartItem(p));
		}
	}

	private void addToPricingRules(String... promoCodes) {
		if (promoCodes != null && promoCodes.length > 0) {
			for (String code : promoCodes) {
				pricingRules.add(pricingRulesRepository.get(code));
			}
		}
	}
}
