package com.amaysim.scart.collector;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import com.amaysim.scart.views.CartItem;
import com.amaysim.scart.views.Product;

public class BundledItemCollector extends AbstractCartListCollector {
	private Product bundledProduct;

	private BundledItemCollector(Product bundledProduct) {
		this.bundledProduct = bundledProduct;
	}

	@Override
	public Function<List<CartItem>, List<CartItem>> finisher() {
		return l -> {
			List<CartItem> r = new ArrayList<>();
			l.forEach(c -> {
				r.add(new CartItem(bundledProduct));
			});
			return r;
		};
	}

	public static BundledItemCollector newInstance(Product product) {
		return new BundledItemCollector(product);
	}

}
