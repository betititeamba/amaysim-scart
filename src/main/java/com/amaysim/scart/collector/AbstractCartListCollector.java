package com.amaysim.scart.collector;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Supplier;
import java.util.stream.Collector;

import com.amaysim.scart.views.CartItem;

public abstract class AbstractCartListCollector implements Collector<CartItem, List<CartItem>, List<CartItem>> {
	
	@Override
	public BiConsumer<List<CartItem>, CartItem> accumulator() {
		return List::add;
	}

	@Override
	public Set<Characteristics> characteristics() {
		return EnumSet.of(Characteristics.UNORDERED);
	}

	@Override
	public BinaryOperator<List<CartItem>> combiner() {
		return (l, r) -> {
			l.addAll(r);
			return l;
		};
	}

	@Override
	public Supplier<List<CartItem>> supplier() {
		return ArrayList::new;
	}

}
