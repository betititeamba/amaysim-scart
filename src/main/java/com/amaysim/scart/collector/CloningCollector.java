package com.amaysim.scart.collector;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import com.amaysim.scart.views.CartItem;

public class CloningCollector extends AbstractCartListCollector {

	private CloningCollector() {
	}

	@Override
	public Function<List<CartItem>, List<CartItem>> finisher() {
		return l -> {
			List<CartItem> clones = new ArrayList<>();
			l.forEach(c -> {
				clones.add(c.copy());
			});
			return clones;
		};
	}

	public static CloningCollector newInstance() {
		return new CloningCollector();
	}

}
