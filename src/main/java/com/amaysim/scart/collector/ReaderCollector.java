package com.amaysim.scart.collector;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

import com.amaysim.scart.linemapper.LineMapper;

public class ReaderCollector<R> implements Collector<String, List<String>, List<R>> {
	private LineMapper<R> lineMapper;

	private ReaderCollector(LineMapper<R> lineMapper) {
		this.lineMapper = lineMapper;
	}

	@Override
	public Function<List<String>, List<R>> finisher() {
		return l -> {
			List<R> result = new ArrayList<>();
			l.forEach(s -> {
				try {
					result.add(lineMapper.map(s.split(",")));
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			});
			return result;
		};
	}

	@Override
	public BiConsumer<List<String>, String> accumulator() {
		return List::add;
	}

	@Override
	public Set<Characteristics> characteristics() {
		return EnumSet.of(Characteristics.UNORDERED);
	}

	@Override
	public BinaryOperator<List<String>> combiner() {
		return (l, r) -> {
			l.addAll(r);
			return l;
		};
	}

	@Override
	public Supplier<List<String>> supplier() {
		return ArrayList::new;
	}

	public static <R> ReaderCollector<R> newInstance(LineMapper<R> lineMapper) {
		return new ReaderCollector<>(lineMapper);
	}
}
