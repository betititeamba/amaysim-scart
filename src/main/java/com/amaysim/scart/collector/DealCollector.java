package com.amaysim.scart.collector;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.function.Function;
import java.util.stream.IntStream;

import com.amaysim.scart.views.CartItem;

public class DealCollector extends AbstractCartListCollector {

	private Integer deal;
	private Integer _for;

	private DealCollector(Integer deal, Integer _for) {
		this.deal = deal;
		this._for = _for;
	}

	@Override
	public Function<List<CartItem>, List<CartItem>> finisher() {
		return l -> {
			return IntStream.range(0, l.size()).filter(this::dealCheck).mapToObj(i -> l.get(i)).collect(toList());
		};
	}

	private boolean dealCheck(Integer i) {
		int modulo = (i + 1) % deal;
		return modulo == 0 || modulo > _for;
	}

	public static DealCollector newInstance(Integer deal, Integer _for) {
		return new DealCollector(deal, _for);
	}
}
