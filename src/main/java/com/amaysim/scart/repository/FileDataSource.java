package com.amaysim.scart.repository;

import static java.lang.ClassLoader.getSystemResource;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Stream;

import com.amaysim.scart.collector.ReaderCollector;
import com.amaysim.scart.linemapper.LineMapper;

public class FileDataSource {

	private static final Logger logger = Logger.getLogger(FileDataSource.class.getName());

	private Stream<String> fileStream;
	private String fileData;

	private FileDataSource(String fileData) {
		this.fileData = fileData;
	}

	public static FileDataSource connect(String fileData) {
		try {
			return new FileDataSource(fileData).connect();
		} catch (Exception e) {
			logger.severe("Unable to load file data source.");
			throw new RuntimeException("Unable to load file data source", e);
		}
	}

	private FileDataSource connect() throws Exception {
		fileStream = fileStreams(fileData);
		return this;
	}

	public <R> List<R> read(LineMapper<R> lineMapper) {
		try (Stream<String> lines = fileStream) {
			return lines.collect(ReaderCollector.newInstance(lineMapper));
		} catch (Exception e) {
			logger.severe("Error reading data file");
			throw new RuntimeException("Error reading data file", e);
		}
	}

	private Stream<String> fileStreams(String fileData) throws Exception {
		return Files.lines(Paths.get(getSystemResource(fileData).toURI()));
	}

}
