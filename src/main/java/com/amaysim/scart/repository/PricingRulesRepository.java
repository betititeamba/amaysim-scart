package com.amaysim.scart.repository;

import static com.amaysim.scart.repository.FileDataSource.connect;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amaysim.scart.businessrules.PricingRule;
import com.amaysim.scart.linemapper.PricingRuleMapper;

public class PricingRulesRepository {
	private Map<String, PricingRule> promos = new HashMap<>();

	private List<PricingRule> autoRules = new ArrayList<>();

	private static PricingRulesRepository _thisInstance;

	private PricingRulesRepository() {
		init();
	}

	public PricingRule get(String id) {
		return promos.get(id);
	}

	public Collection<PricingRule> getAllAuto() {
		return autoRules;
	}

	public static PricingRulesRepository getInstance() {
		if (_thisInstance == null) {
			_thisInstance = new PricingRulesRepository();
		}
		return _thisInstance;
	}

	private void init() {
		connect("pricingRules").read(new PricingRuleMapper()).forEach(this::add);
	}

	private void addToAutoRules(PricingRule rule) {
		if (rule.isAutoPromo()) {
			autoRules.add(rule);
		}
	}

	private void addToAll(PricingRule rule) {
		promos.put(rule.getRuleCode(), rule);
	}

	private void add(PricingRule rule) {
		addToAll(rule);
		addToAutoRules(rule);
	}
}
