package com.amaysim.scart.repository;

import static java.lang.Double.parseDouble;
import static com.amaysim.scart.repository.FileDataSource.connect;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.amaysim.scart.views.Product;

public class ProductCatalogueRepository {
	private static final Map<String, Product> catalogue = new HashMap<>();

	private static ProductCatalogueRepository _thisInstance;

	private ProductCatalogueRepository() {
		init();
	}

	public static ProductCatalogueRepository getInstance() {
		if (_thisInstance == null) {
			_thisInstance = new ProductCatalogueRepository();
		}
		return _thisInstance;
	}

	private void init() {
		connect("productCatalogue").read(this::map).forEach(this::add);
	}

	private Product map(String[] c) {
		return new Product(c[0], c[1], parseDouble(c[2]));
	}

	private void add(Product p) {
		catalogue.put(p.getCode(), p);
	}

	public Product getProduct(String code) {
		return catalogue.get(code);
	}

	public Collection<Product> getAll() {
		return catalogue.values();
	}
}
