package com.amaysim.scart.businessrules;

import java.util.List;

import com.amaysim.scart.collector.BundledItemCollector;
import com.amaysim.scart.views.CartItem;
import com.amaysim.scart.views.Product;

public class BundledPricingRule extends AbstractPricingRule {

	private Product bundled;
	private String bundledProductCode;
	private Double bundledPrice;

	public BundledPricingRule(Product bundled, String bundledProductCode, Double bundledPrice, String ruleCode,
			Boolean autoPromo) {
		super(ruleCode, autoPromo);
		this.bundled = bundled;
		this.bundledProductCode = bundledProductCode;
		this.bundledPrice = bundledPrice;
	}

	@Override
	public void identify(List<CartItem> items) {
		if (items == null)
			return;
		BundledItemCollector c = BundledItemCollector.newInstance(bundled);
		List<CartItem> bItems = items.stream().filter(this::bundledCheck).collect(c);
		bItems.forEach(this::addThisRule);
		items.addAll(bItems);
	}

	@Override
	public Double calculate(Double productPrice) {
		return bundledPrice;
	}

	private boolean bundledCheck(CartItem i) {
		return bundledProductCode.equals(i.getProduct().getCode());
	}
}
