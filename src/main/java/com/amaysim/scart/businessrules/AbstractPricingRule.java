package com.amaysim.scart.businessrules;

import com.amaysim.scart.views.CartItem;

public abstract class AbstractPricingRule implements PricingRule {
	private String ruleCode;
	private Boolean autoPromo;

	protected AbstractPricingRule(String ruleCode, Boolean autoPromo) {
		super();
		this.ruleCode = ruleCode;
		this.autoPromo = autoPromo;
	}

	@Override
	public boolean isAutoPromo() {
		return autoPromo;
	}

	@Override
	public String getRuleCode() {
		return ruleCode;
	}
	
	protected void addThisRule(CartItem i) {
		i.addPricingRule(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ruleCode == null) ? 0 : ruleCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractPricingRule other = (AbstractPricingRule) obj;
		if (ruleCode == null) {
			if (other.ruleCode != null)
				return false;
		} else if (!ruleCode.equals(other.ruleCode))
			return false;
		return true;
	}

}
