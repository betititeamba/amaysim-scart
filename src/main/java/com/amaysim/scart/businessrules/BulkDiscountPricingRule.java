package com.amaysim.scart.businessrules;

import static java.util.stream.Collectors.toList;

import java.util.List;

import com.amaysim.scart.views.CartItem;

public class BulkDiscountPricingRule extends AbstractPricingRule {

	private Double bulkPrice;
	private Integer bulkSize;
	private String bulkProductCode;

	public BulkDiscountPricingRule(Double bulkPrice, Integer bulkSize, String bulkProductCode, String ruleCode,
			Boolean autoPromo) {
		super(ruleCode, autoPromo);
		this.bulkPrice = bulkPrice;
		this.bulkSize = bulkSize;
		this.bulkProductCode = bulkProductCode;
	}

	@Override
	public void identify(List<CartItem> items) {
		if (items == null)
			return;
		List<CartItem> _bulkList = items.stream().filter(this::bulkCheck).collect(toList());
		if (_bulkList.size() > bulkSize) {
			_bulkList.forEach(this::addThisRule);
		}
	}

	@Override
	public Double calculate(Double productPrice) {
		return bulkPrice;
	}

	private boolean bulkCheck(CartItem i) {
		return bulkProductCode.equals(i.getProduct().getCode());
	}

}
