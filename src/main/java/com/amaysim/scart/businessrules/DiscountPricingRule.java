package com.amaysim.scart.businessrules;

import java.util.List;

import com.amaysim.scart.views.CartItem;

public class DiscountPricingRule extends AbstractPricingRule {

	private Double percentage;

	public DiscountPricingRule(Double percentage, String ruleCode, Boolean autoPromo) {
		super(ruleCode, autoPromo);
		this.percentage = percentage;
	}

	@Override
	public void identify(List<CartItem> items) {
		if (items == null)
			return;
		items.forEach(this::addThisRule);
	}

	@Override
	public Double calculate(Double productPrice) {
		return productPrice - ((percentage * productPrice) / 100);
	}
}
