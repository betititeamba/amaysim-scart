package com.amaysim.scart.businessrules;

import java.util.List;

import com.amaysim.scart.collector.DealCollector;
import com.amaysim.scart.views.CartItem;

public class DealPricingRule extends AbstractPricingRule {

	private Integer deal;
	private Integer _for;
	private String dealProductCode;
	private Double dealPrice;

	public DealPricingRule(Integer deal, Integer _for, String dealProductCode, Double dealPrice, String ruleCode,
			Boolean autoPromo) {
		super(ruleCode, autoPromo);
		this.deal = deal;
		this._for = _for;
		this.dealProductCode = dealProductCode;
		this.dealPrice = dealPrice;
	}

	@Override
	public void identify(List<CartItem> items) {
		if (items == null)
			return;
		DealCollector c = DealCollector.newInstance(deal, _for);
		items.stream().filter(this::dealCheck).collect(c).forEach(this::addThisRule);
	}

	@Override
	public Double calculate(Double productPrice) {
		return dealPrice;
	}

	private boolean dealCheck(CartItem i) {
		return dealProductCode.equals(i.getProduct().getCode());
	}

}
