package com.amaysim.scart.businessrules;

import java.util.List;

import com.amaysim.scart.views.CartItem;

public interface PricingRule {

	/**
	 * Attach rule to item if calculated to be one. Can also alter existing cart
	 * based on pricing rule logic.
	 * 
	 * @param items
	 */
	void identify(List<CartItem> items);

	/**
	 * Will calculate the pricing
	 * 
	 * @param item
	 * @return
	 */
	Double calculate(Double productPrice);

	/**
	 * Will identify if rules belong to auto calculation or manual calculation
	 * prior to total calculation.
	 * 
	 * @return
	 */
	boolean isAutoPromo();

	/**
	 * Provide the rule code
	 * 
	 * @return
	 */
	String getRuleCode();
}
