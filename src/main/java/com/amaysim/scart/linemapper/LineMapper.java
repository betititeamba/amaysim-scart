package com.amaysim.scart.linemapper;

@FunctionalInterface
public interface LineMapper<R> {
	R map(String[] c) throws Exception;
}
