package com.amaysim.scart.linemapper;

import static java.util.stream.Collectors.toList;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

import com.amaysim.scart.businessrules.BulkDiscountPricingRule;
import com.amaysim.scart.businessrules.BundledPricingRule;
import com.amaysim.scart.businessrules.DealPricingRule;
import com.amaysim.scart.businessrules.DiscountPricingRule;
import com.amaysim.scart.businessrules.PricingRule;
import com.amaysim.scart.repository.ProductCatalogueRepository;
import com.amaysim.scart.views.Product;

public class PricingRuleMapper implements LineMapper<PricingRule> {

	private static final ProductCatalogueRepository pcRepository = ProductCatalogueRepository.getInstance();

	private static final Map<String, Class<? extends PricingRule>> rulesType = new HashMap<>();

	static {
		rulesType.put("deal", DealPricingRule.class);
		rulesType.put("bulk", BulkDiscountPricingRule.class);
		rulesType.put("bundled", BundledPricingRule.class);
		rulesType.put("discount", DiscountPricingRule.class);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public PricingRule map(String[] c) throws Exception {
		Constructor cons = rulesType.get(c[0]).getConstructors()[0];
		return ((PricingRule) cons.newInstance(params(c, cons)));
	}

	@SuppressWarnings("rawtypes")
	private Object[] params(String[] c, Constructor cons) throws Exception {
		Class<?>[] paramTypes = cons.getParameterTypes();
		return IntStream.range(0, paramTypes.length).mapToObj(i -> {
			return object(c, paramTypes, i);
		}).collect(toList()).toArray();
	}

	private Object object(String[] c, Class<?>[] paramTypes, int i) {
		try {
			Object o;
			Class<?> p = paramTypes[i];
			if (p.isInstance(new Product())) {
				o = pcRepository.getProduct(c[i + 1]);
			} else {
				o = p.getConstructor(String.class).newInstance(c[i + 1]);
			}
			return o;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
