package com.amaysim.scart.test.steps;

import static com.amaysim.scart.views.Product.ONE_GB_CODE;
import static com.amaysim.scart.views.Product.UNLIMITED_1GB_CODE;
import static com.amaysim.scart.views.Product.UNLIMITED_2GB_CODE;
import static com.amaysim.scart.views.Product.UNLIMITED_5GB_CODE;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.IntStream;

import com.amaysim.scart.repository.ProductCatalogueRepository;
import com.amaysim.scart.service.ShoppingCart;
import com.amaysim.scart.views.CartItem;
import com.amaysim.scart.views.Product;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * 
 * Cucumber step definitions
 *
 */
public class PromoStepDefinitions {

	private static final DecimalFormat dollarFormat = new DecimalFormat("$#.00");

	private static final ProductCatalogueRepository productCatalogueRepository = ProductCatalogueRepository
			.getInstance();

	private ShoppingCart shoppingCart;

	private List<Product> items = new ArrayList<>();

	private Consumer<Product> cartHandler;

	@Before
	public void setUp() throws Throwable {
		shoppingCart = ShoppingCart.newInstance();
		items = new ArrayList<>();
		cartHandler = p -> shoppingCart.add(p);
	}

	@After
	public void tearDown() throws Throwable {
		shoppingCart = null;
		items = null;
		cartHandler = null;
	}

	@Given("^(\\d+) x Unlimited 1 GB$")
	public void x_Unlimited_1GB(int count) throws Throwable {
		items.addAll(collect(count, getUnli1Gb()));
	}

	@Given("^(\\d+) x Unlimited 2 GB$")
	public void x_Unlimited_2GB(int count) throws Throwable {
		items.addAll(collect(count, getUnli2Gb()));
	}

	@Given("^(\\d+) x Unlimited 5 GB$")
	public void x_Unlimited_5GB(int count) throws Throwable {
		items.addAll(collect(count, getUnli5Gb()));
	}

	@Given("^(\\d+) x 1 GB Data-pack$")
	public void x_GB_Data_pack(int count) throws Throwable {
		items.addAll(collect(count, get1GbDataPack()));
	}

	@Given("^apply a promo code of \"([^\"]*)\"$")
	public void apply_a_promo_code_of(String promoCode) throws Throwable {
		cartHandler = p -> shoppingCart.add(p, promoCode);
	}

	@When("^they are all added to cart$")
	public void they_are_all_added_to_cart() throws Throwable {
		items.forEach(cartHandler);
	}

	@Then("^it will provide a total price of (\\d+)\\.(\\d+)$")
	public void it_will_provide_a_total_price_of(int w, int d) throws Throwable {
		double price = (double) w + ((double) d / 100);
		double total = shoppingCart.total();
		assertEquals(dollarFormat.format(price), dollarFormat.format(total));
	}

	@Then("^expected cart items of (\\d+) x Unlimited 1 GB$")
	public void expected_cart_items_of_x_Unlimited_1GB(int count) throws Throwable {
		assertEquals(count, count(this::isUnli1Gb));
	}

	@Then("^expected cart items of (\\d+) x Unlimited 2 GB$")
	public void expected_cart_items_of_x_Unlimited_2GB(int count) throws Throwable {
		assertEquals(count, count(this::isUnli2Gb));
	}

	@Then("^expected cart items of (\\d+) x Unlimited 5 GB$")
	public void expected_cart_items_of_x_Unlimited_5GB(int count) throws Throwable {
		assertEquals(count, count(this::isUnli5Gb));
	}

	@Then("^expected cart items of (\\d+) x 1 GB Data-pack$")
	public void expected_cart_items_of_x_GB_Data_pack(int count) throws Throwable {
		assertEquals(count, count(this::is1Gb));
	}

	private boolean isUnli1Gb(CartItem i) {
		return UNLIMITED_1GB_CODE.equals(i.getProduct().getCode());
	}

	private boolean isUnli2Gb(CartItem i) {
		return UNLIMITED_2GB_CODE.equals(i.getProduct().getCode());
	}

	private boolean isUnli5Gb(CartItem i) {
		return UNLIMITED_5GB_CODE.equals(i.getProduct().getCode());
	}

	private boolean is1Gb(CartItem i) {
		return ONE_GB_CODE.equals(i.getProduct().getCode());
	}

	private Product getUnli1Gb() {
		return productCatalogueRepository.getProduct(UNLIMITED_1GB_CODE);
	}

	private Product getUnli2Gb() {
		return productCatalogueRepository.getProduct(UNLIMITED_2GB_CODE);
	}

	private Product getUnli5Gb() {
		return productCatalogueRepository.getProduct(UNLIMITED_5GB_CODE);
	}

	private Product get1GbDataPack() {
		return productCatalogueRepository.getProduct(ONE_GB_CODE);
	}

	private List<Product> collect(int count, Product p) {
		return IntStream.range(0, count).mapToObj(i -> p).collect(toList());
	}

	private int count(Predicate<CartItem> predicate) {
		return (int) shoppingCart.items().stream().filter(predicate).count();
	}
}
