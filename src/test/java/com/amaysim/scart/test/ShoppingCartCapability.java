package com.amaysim.scart.test;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * jUnit cucumber test
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:features/shoppingcartpromo.feature")
public class ShoppingCartCapability {
}
