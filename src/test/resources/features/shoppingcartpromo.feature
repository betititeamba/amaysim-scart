Feature:
      As the market tends to fluctuate in terms of pricing, the rules around this need to be as flexible as possible as they can change with little notice.
	  Customers can add items to the cart in any order.

 Scenario:
	A 3 for 2 deal on Unlimited 1GB Sims. So for example, if you buy 3 Unlimited 1GB Sims, you will pay the price of 2 only for the first month.
	Given 3 x Unlimited 1 GB
      And 1 x Unlimited 5 GB
     When they are all added to cart
     Then it will provide a total price of 94.70
      And expected cart items of 3 x Unlimited 1 GB
      And expected cart items of 1 x Unlimited 5 GB
        
        
 Scenario:
 	The Unlimited 5GB Sim will have a bulk discount applied; whereby the price will drop to $39.90 each for the first month, if the customer buys more than 3.
 	Given 2 x Unlimited 1 GB
 	  And 4 x Unlimited 5 GB
     When they are all added to cart
 	 Then it will provide a total price of 209.40
      And expected cart items of 2 x Unlimited 1 GB
      And expected cart items of 4 x Unlimited 5 GB
 	  
Scenario:
	We will bundle in a free 1 GB Data-pack free-of-charge with every Unlimited 2GB sold.
	Given 1 x Unlimited 1 GB
	  And 2 x Unlimited 2 GB
     When they are all added to cart
 	 Then it will provide a total price of 84.70
      And expected cart items of 1 x Unlimited 1 GB
      And expected cart items of 2 x Unlimited 2 GB
      And expected cart items of 2 x 1 GB Data-pack
	 	
Scenario:
	Adding the promo code 'I<3AMAYSIM' will apply a 10% discount across the board.
	Given 1 x Unlimited 1 GB
	  And 1 x 1 GB Data-pack
	  And apply a promo code of "I<3AMAYSIM"
     When they are all added to cart
	 Then it will provide a total price of 31.32
	  And expected cart items of 1 x Unlimited 1 GB
	  And expected cart items of 1 x 1 GB Data-pack
	
	 	